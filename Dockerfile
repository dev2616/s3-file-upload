FROM golang:latest as builder
RUN mkdir /app 
ADD . /app/ 
WORKDIR /app 
EXPOSE 4000
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -tags netgo -ldflags '-w' -o main *.go

#поднимаем бинарник
FROM alpine

COPY --from=builder /app/main /main
COPY --from=builder /app/.aws /root/.aws/

ENTRYPOINT [ "/main" ]