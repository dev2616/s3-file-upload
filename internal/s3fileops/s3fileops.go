package s3fileops

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/feature/s3/manager"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/aws/aws-sdk-go-v2/service/s3/types"
	"github.com/gin-gonic/gin"
)

type S3File struct {
	Filename           string
	Filepath           string
	FileBuffer         *bytes.Buffer
	Bucket             string
	ACL                string
	ContentDisposition string
}

func S3FileInit(contentdisposition, bucket, acl string, filebuffer *bytes.Buffer) *S3File {
	return &S3File{
		//	Filename:   filename,
		FileBuffer:         filebuffer,
		ACL:                acl,
		Bucket:             bucket,
		ContentDisposition: contentdisposition,
	}
}

func (s3f S3File) s3FileExists(c *gin.Context, fileName string) (bool, error) {
	client := c.MustGet("client").(*s3.Client)

	_, err := client.HeadObject(c, &s3.HeadObjectInput{
		Bucket: aws.String(s3f.Bucket),
		Key:    aws.String(fileName),
	})

	if err != nil {
		return false, err
	}

	return true, nil
}

func (s3f S3File) DownloadFile(c *gin.Context) {
	client := c.MustGet("client").(*s3.Client)
	fileName := c.Param("fileName")

	_, err := s3f.s3FileExists(c, fileName)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"message": "File not found"})
		return
	}

	downloader := manager.NewDownloader(client)
	input := &s3.GetObjectInput{
		Bucket: aws.String(s3f.Bucket),
		Key:    aws.String(fileName),
	}

	buf := manager.NewWriteAtBuffer([]byte{})

	_, err = downloader.Download(c, buf, input)

	if err != nil {
		log.Println(err)
	}

	c.Data(http.StatusOK, "application/json", buf.Bytes())
}

func (s3f S3File) UploadFile(c *gin.Context) {
	client := c.MustGet("client").(*s3.Client)

	file, header, err := c.Request.FormFile("file")
	fmt.Println("1")
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}

	filename := header.Filename

	s3f.Filename = filename

	if _, err := io.Copy(s3f.FileBuffer, file); err != nil {
		log.Println(err)
		os.Exit(1)
	}

	params := &s3.PutObjectInput{
		Bucket:             aws.String(s3f.Bucket),
		Key:                aws.String(s3f.Filename),
		ACL:                types.ObjectCannedACL(*aws.String(s3f.ACL)),
		Body:               bytes.NewReader(s3f.FileBuffer.Bytes()),
		ContentDisposition: aws.String(s3f.ContentDisposition),
	}

	resp, err := client.PutObject(context.TODO(), params)
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}

	fmt.Println(resp)
}
