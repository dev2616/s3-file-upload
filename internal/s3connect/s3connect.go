package s3connect

import (
	"context"
	"fmt"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/s3"
)

type S3Cred struct {
	Regin       string
	URL         string
	PartitionID string
}

func S3CredInit(region, url, partitionid string) *S3Cred {
	return &S3Cred{
		Regin:       region,
		URL:         url,
		PartitionID: partitionid,
	}
}

func (s3cred *S3Cred) CustomResolver() aws.EndpointResolverWithOptionsFunc {
	return aws.EndpointResolverWithOptionsFunc(func(service, region string, options ...interface{}) (aws.Endpoint, error) {
		if service == s3.ServiceID && region == s3cred.Regin {
			return aws.Endpoint{
				PartitionID:   s3cred.PartitionID,
				URL:           s3cred.URL,
				SigningRegion: s3cred.Regin,
			}, nil
		}
		return aws.Endpoint{}, fmt.Errorf("unknown endpoint requested")
	})
}

func (s3cred *S3Cred) LoadConfig(customresolver aws.EndpointResolverWithOptionsFunc) (aws.Config, error) {
	cfg, err := config.LoadDefaultConfig(context.TODO(), config.WithEndpointResolverWithOptions(customresolver))
	if err != nil {
		return cfg, err
	}
	return cfg, nil
}

func (s3cred *S3Cred) ClientInit(cfg aws.Config) *s3.Client {
	return s3.NewFromConfig(cfg)
}
