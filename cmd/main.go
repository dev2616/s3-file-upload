package main

import (
	"bytes"
	"fmt"
	"log"

	"github.com/gin-gonic/gin"
	"gitlab.com/dev2616/s3-file-upload/internal/s3connect"
	"gitlab.com/dev2616/s3-file-upload/internal/s3fileops"
)

func main() {

	region := "ru-central1"
	url := "https://storage.yandexcloud.net"
	partitionid := "yc"
	bucket := "json-files-upload"
	filebuffer := bytes.NewBuffer(nil)
	acl := "public-read-write"
	contentdisposition := "attachment"

	s3cred := s3connect.S3CredInit(region, url, partitionid)
	customResolver := s3cred.CustomResolver()
	cfg, err := s3cred.LoadConfig(customResolver)
	if err != nil {
		log.Fatal(err)
	}
	client := s3cred.ClientInit(cfg)

	fmt.Println(client)

	s3file := s3fileops.S3FileInit(contentdisposition, bucket, acl, filebuffer)

	router := gin.Default()
	router.Use(func(ctx *gin.Context) {
		ctx.Set("client", client)
		ctx.Next()
	})

	router.POST("/upload", s3file.UploadFile)
	router.GET("/download/:fileName", s3file.DownloadFile)
	_ = router.Run(":4000")
}
